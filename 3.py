class Node:
    def __init__(self, value, parent):
        self.value = value
        self.parent = parent


def lca(node_a, node_b):
    ancestors = {}
    while(node_a):
        ancestors[node_a.value] = 1
        node_a=node_a.parent

    while(node_b):
        if node_b.value in ancestors.keys():
            return node_b
        node_b = node_b.parent
    return None

node_1 = Node(1, None)
node_2 = Node(2, node_1)
node_3 = Node(3, node_1)
node_4 = Node(4, node_2)
node_5 = Node(5, node_2)
node_6 = Node(6, node_3)
node_7 = Node(7, node_3)
node_8 = Node(8, node_4)
node_9 = Node(9, node_4)

node_lca = lca(node_7, None)
if node_lca:
    print(node_lca.value)