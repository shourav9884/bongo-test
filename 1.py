def print_depth_recursive(data, depth):
	for k,v in data.items():
		print("{key}: {depth}".format(key=k, depth=depth))
		if isinstance(v, dict):
			print_depth_recursive(v, depth+1)


def print_depth(data):
	print_depth_recursive(data, 1)


data = {"key1":1, "key2":{"key3":{"key6":{"key7":8}, "key8":9}, "key4":{"key5":4}}}
print_depth(data)