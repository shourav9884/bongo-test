class Person(object):
    def __init__(self, first_name, last_name, father):
        self.first_name = first_name
        self.last_name = last_name
        self.father = father

def print_depth_recursive(data, depth):
    for k, v in data.items():
        print("{key}: {depth}".format(key=k, depth=depth))
        if isinstance(v, dict):
            print_depth_recursive(v, depth + 1)
        elif isinstance(v, Person):
            print_depth_recursive(v.__dict__, depth+1)


def print_depth(data):
    print_depth_recursive(data, 1)


person_a = Person("a", "b", None)
person_b = Person("a", "b", person_a)

data = {"key1":1, "key2":{"key3":1, "key4":{"key5":4, "user": person_b}}}
print_depth(data)

